import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

n = int(input())
vs = input()

max_so_far = 0
max_loss = 0

for v in [int(v) for v in vs.split()]:
    print(v, file=sys.stderr)
    loss = max_so_far - v
    if loss > max_loss:
        max_loss = loss
    if v > max_so_far:
        max_so_far = v

    """
        loss = v - min_so_far
        if loss > max_loss:
            max_loss = loss
        max_so_far = v
    if v < min_so_far:
        loss = max_so_far - v
        if loss > max_loss:
            max_loss = loss
        min_so_far = v
    """

# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)

print(max_loss*-1)
