import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

TOP = "TOP"
LEFT = "LEFT"
RIGHT = "RIGHT"
BOTTOM = "BOTTOM"
DEATH = "DEATH"

class Tile(object):
    def __init__(self):
        self.paths = {}

    def add_path(self, entrance, exit):
        self.paths[entrance] = exit
        return self

TILES = [Tile(), # Type 0
         Tile().add_path(LEFT, BOTTOM).add_path(TOP, BOTTOM).add_path(RIGHT, BOTTOM), # Type 1
         Tile().add_path(LEFT, RIGHT).add_path(RIGHT, LEFT), # Type 2
         Tile().add_path(TOP, BOTTOM), # Type 3
         Tile().add_path(TOP, LEFT).add_path(RIGHT, BOTTOM).add_path(LEFT, DEATH), # Type 4
         Tile().add_path(TOP, RIGHT).add_path(LEFT, BOTTOM).add_path(RIGHT, DEATH), # Type 5
         Tile().add_path(LEFT, RIGHT).add_path(RIGHT, LEFT).add_path(TOP, DEATH), # Type 6
         Tile().add_path(TOP, BOTTOM).add_path(RIGHT, BOTTOM), # Type 7
         Tile().add_path(LEFT, BOTTOM).add_path(RIGHT, BOTTOM), # Type 8
         Tile().add_path(LEFT, BOTTOM).add_path(TOP, BOTTOM), # Type 9
         Tile().add_path(TOP, LEFT).add_path(LEFT, DEATH), # Type 10
         Tile().add_path(TOP, RIGHT).add_path(RIGHT, DEATH), # Type 11
         Tile().add_path(RIGHT, BOTTOM), # Type 12
         Tile().add_path(LEFT, BOTTOM)] # Type 12

class Grid(object):
    def __init__(self):
        self.rooms = []
        self.nrows = 0

    def add_row(self, rooms):
        row = []
        for col, room_type in enumerate(rooms):
            row.append(Room(Position(col, self.nrows), room_type))
        self.rooms.append(row)
        self.nrows += 1

    def get_room(self, x, y):
        return self.rooms[y][x]

class Position(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Room(object):
    def __init__(self, position, room_type):
        global TILES
        self.tile = TILES[int(room_type)]
        self.position = position

    def enter(self, entrance):
        exit = self.tile.paths[entrance]
        if exit == "BOTTOM":
            exit_to = Position(self.position.x, self.position.y+1)
        elif exit == "LEFT":
            exit_to = Position(self.position.x-1, self.position.y)
        elif exit == "RIGHT":
            exit_to = Position(self.position.x+1, self.position.y)
        else:
            exit_to = None # dead. there is no exit..!
        return exit_to

grid = Grid()
# W: number of columns.
# H: number of rows.
W, H = [int(i) for i in input().split()]
for i in range(H):
    LINE = input() # represents a line in the grid and contains W integers. Each integer represents one room of a given type.
    grid.add_row(LINE.split())

sys.stderr.write(str(grid.rooms) + "\n")
EX = int(input()) # the coordinate along the X axis of the exit (not useful for this first mission, but must be read).

# game loop
while 1:
    XI, YI, POS = input().split()
    XI = int(XI)
    YI = int(YI)

    room = grid.get_room(XI, YI)
    next_room_pos = room.enter(POS)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    print("%d %d" % (next_room_pos.x, next_room_pos.y)) # One line containing the X Y coordinates of the room in which you believe Indy will be on the next turn.
