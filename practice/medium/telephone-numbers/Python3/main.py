import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

class NumberNode(object):
    def __init__(self):
        self.children = {}
        
    def add_number(self, number):
        count = 0
        next_digit = number[0]
        if next_digit not in self.children:
            self.children[next_digit] = NumberNode()
            count += 1
        if len(number) > 1:
            count += self.children[next_digit].add_number(number[1:])
        return count

root = NumberNode()
count = 0

N = int(input())
for i in range(N):
    telephone = input()
    count += root.add_number(telephone)

# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)

print(count) # The number of elements (referencing a number) stored in the structure.
