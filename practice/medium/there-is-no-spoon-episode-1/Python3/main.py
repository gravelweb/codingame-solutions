import sys, math

# Don't let the machines win. You are humanity's last hope...

rows = []
y_neighbours = {}

X=0
Y=1

matches = []
def store_match(match):
    global matches
    matches.append(match)

def print_matches():
    global matches
    for match in matches:
        print('{} {} {} {} {} {}'.format(*match))

width = int(input()) # the number of cells on the X axis
height = int(input()) # the number of cells on the Y axis
for i in range(height):
    row = input()
    x_neighbour = None
    for j, item in enumerate(row):
        node = [j, i]
        if item == '0':
            if j in y_neighbours:
                y_neighb = y_neighbours[j]
                if len(y_neighb) == 2:
                    y_neighb.extend([-1, -1])  # no x neighbour
                y_neighb.extend(node)
                store_match(y_neighb)
            y_neighbours[j] = node
            if x_neighbour is not None:
                print(x_neighbour[Y], file=sys.stderr)
                y_neighbours[x_neighbour[X]].extend(node)
                print(y_neighbours, file=sys.stderr)
            x_neighbour = node

print(matches, file=sys.stderr)

for y_neighb in y_neighbours.values():
    print(y_neighb, file=sys.stderr)
    while len(y_neighb) != 6:
        y_neighb.extend([-1]*2)
    store_match(y_neighb)

# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)

print_matches()
