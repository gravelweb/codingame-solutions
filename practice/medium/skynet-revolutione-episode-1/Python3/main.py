import sys, math
from collections import deque

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

class Link(object):
    def __init__(self, A, B):
        self.A = A
        self.B = B
        self.current = None
        self.blocked = False
    
    @property
    def other(self):
        if self.current == self.A:
            return self.B
        else:
            return self.A
        
class Node(object):
    def __init__(self, id):
        self.id = id
        self.links = []
        self.gateway = False

nodes = {}

# N: the total number of nodes in the level, including the gateways
# L: the number of links
# E: the number of exit gateways
N, L, E = [int(i) for i in input().split()]
for i in range(L):
    # N1: N1 and N2 defines a link between these nodes
    N1, N2 = [int(i) for i in input().split()]
    if N1 not in nodes:
        nodes[N1] = Node(N1)
    if N2 not in nodes:
        nodes[N2] = Node(N2)

    link = Link(N1, N2)
    nodes[N1].links.append(link)
    nodes[N2].links.append(link)

for i in range(E):
    EI = int(input()) # the index of a gateway node
    nodes[EI].gateway = True

# game loop
while 1:
    SI = int(input()) # The index of the node on which the Skynet agent is positioned this turn
    
    current = SI
    
    visiting = deque()
    for link in nodes[current].links:
        link.current = current
        visiting.append(link)
    visited = []
    
    for link in visiting:
        if (nodes[link.current].gateway or nodes[link.other].gateway) and link.blocked == False:
            print('%d %d' % (link.A, link.B))
            link.blocked = True
            break
    else:
        while visiting:
            link = visiting.popleft()
            if (len(nodes[link.current].links) <= 3 or len(nodes[link.other].links) <= 3) and link.blocked == False:
                print('%d %d' % (link.A, link.B))
                link.blocked = True
                break
                
            visited.append(link)
            dest = link.other
            for sublink in nodes[dest].links:
                if sublink not in visiting and sublink not in visiting:
                    sublink.current = dest
                    visiting.append(sublink)
"""
while 1:
    SI = int(input()) # The index of the node on which the Skynet agent is positioned this turn
    
    current = SI
    
    visiting = deque()
    for link in nodes[current].links:
        link.current = current
        visiting.append(link)
    visited = []
    
    while visiting:
        link = visiting.popleft()
        if (nodes[link.current].gateway or nodes[link.other].gateway) and link.blocked == False:
            print('%d %d' % (link.A, link.B))
            link.blocked = True
            break
            
        visited.append(link)
        dest = link.other
        for sublink in nodes[dest].links:
            if sublink not in visiting and sublink not in visiting:
                sublink.current = dest
                visiting.append(sublink)
"""
