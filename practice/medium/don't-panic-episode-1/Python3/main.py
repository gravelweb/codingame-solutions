import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.


# nbFloors: number of floors
# width: width of the area
# nbRounds: maximum number of rounds
# exitFloor: floor on which the exit is found
# exitPos: position of the exit on its floor
# nbTotalClones: number of generated clones
# nbAdditionalElevators: ignore (always zero)
# nbElevators: number of elevators
nbFloors, width, nbRounds, exitFloor, exitPos, nbTotalClones, nbAdditionalElevators, nbElevators = [int(i) for i in input().split()]

floors = [None for _ in range(nbFloors)]
for i in range(nbElevators):
    # elevatorFloor: floor on which this elevator is found
    # elevatorPos: position of the elevator on its floor
    elevatorFloor, elevatorPos = [int(i) for i in input().split()]
    floors[elevatorFloor] = elevatorPos

# game loop
while 1:
    # cloneFloor: floor of the leading clone
    # clonePos: position of the leading clone on its floor
    # direction: direction of the leading clone: LEFT or RIGHT
    cloneFloor, clonePos, direction = input().split()
    cloneFloor = int(cloneFloor)
    clonePos = int(clonePos)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)
    if cloneFloor != exitFloor:
        if direction == "RIGHT" and clonePos > floors[cloneFloor]:
            print("BLOCK")
        elif direction == "LEFT" and clonePos < floors[cloneFloor]:
            print("BLOCK")
        #if clonePos != floors[cloneFloor] and (
        #        clonePos == width-1 or clonePos == 0):
        #    print("BLOCK")
        else:
            print("WAIT") # action: WAIT or BLOCK
    else:
        if direction == "RIGHT" and clonePos > exitPos:
            print("BLOCK")
        elif direction == "LEFT" and clonePos < exitPos:
            print("BLOCK")
        #if clonePos != floors[cloneFloor] and (
        #        clonePos == width-1 or clonePos == 0):
        #    print("BLOCK")
        else:
            print("WAIT") # action: WAIT or BLOCK
