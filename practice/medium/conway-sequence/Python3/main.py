import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

R = int(input())
L = int(input())

# Write an action using print
# To debug: print("Debug messages...", file=sys.stderr)

line = [R]
for i in range(1,L):
    #print(line, file=sys.stderr)
    previous = None
    count = 0
    next_line = []
    for item in line:
        #print("item: %d, previous: %d" % (item, previous if previous is not None else -1), file=sys.stderr)
        if previous == None:
            previous = item
        elif item != previous:
            next_line.append(count)
            next_line.append(previous)
            count = 0
            previous = item
        count += 1
    next_line.append(count)
    next_line.append(previous)
    line = next_line

#print(line, file=sys.stderr)
print(" ".join(str(item) for item in line))
