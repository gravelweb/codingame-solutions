import sys, math

# Auto-generated code below aims at helping you parse
# the standard input according to the problem statement.

# W: width of the building.
# H: height of the building.
W, H = [int(i) for i in input().split()]
N = int(input()) # maximum number of turns before game over.
X0, Y0 = [int(i) for i in input().split()]

class Board(object):
    def __init__(self, x, y, width, height):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

    @property
    def middle(self):
        x = (self.x + (self.x + self.width)) / 2
        y = (self.y + (self.y + self.height)) / 2
        return (x, y)

    def __str__(self):
        return '%d %d %d %d' % (self.x, self.y, self.width, self.height)

board = Board(0, 0, W, H)

# game loop
while 1:
    BOMB_DIR = input() # the direction of the bombs from batman's current location (U, UR, R, DR, D, DL, L or UL)

    # Write an action using print
    # To debug: print("Debug messages...", file=sys.stderr)

    if BOMB_DIR == 'U':
        board.height = Y0 - board.y
    elif BOMB_DIR == 'UR':
        board.width = board.width - (X0 - board.x)
        board.x = X0
        board.height = Y0 - board.y
    elif BOMB_DIR == 'R':
        board.width = board.width - (X0 - board.x)
        board.x = X0
    elif BOMB_DIR == 'DR':
        board.width = board.width - (X0 - board.x)
        board.x = X0
        board.height = board.height - (Y0 - board.y)
        board.y = Y0
    elif BOMB_DIR == 'D':
        board.height = board.height - (Y0 - board.y)
        board.y = Y0
    elif BOMB_DIR == 'DL':
        board.width = X0 - board.x
        board.height = board.height - (Y0 - board.y)
        board.y = Y0
    elif BOMB_DIR == 'L':
        board.width = X0 - board.x
    elif BOMB_DIR == 'UL':
        board.width = X0 - board.x
        board.height = Y0 - board.y

    print(board, file=sys.stderr)
    X0, Y0 = board.middle

    print("%d %d" % (X0, Y0)) # the location of the next window Batman should jump to.
