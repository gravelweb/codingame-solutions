import math
import sys

# pylint: disable=too-few-public-methods
# pylint: disable=invalid-name

class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

class Planet():
    GRAVITY = 3.711
    WIDTH = 7000
    HEIGHT = 3000
    LANDING_AREA_W = 1000

    def __init__(self):
        self.points = []
        self.flat = None

    def add_point(self, x, y):
        try:
            prev_point = self.points[-1]
            if prev_point.y == y:
                self.flat = [prev_point, Point(x, y)]
        except IndexError:
            pass

        self.points.append(Point(x, y))

    @property
    def flat_left(self):
        return self.flat[0]

    @property
    def flat_right(self):
        return self.flat[1]

    @property
    def flat_middle(self):
        return Point((self.flat_left.x+self.flat_right.x)/2,
                     (self.flat_left.y+self.flat_right.y)/2)

class Ship():
    def __init__(self):
        self.position = Point(0, 0)
        self.speed = Point(0, 0)
        self.rotation = 0
        self.thrust = 0
        self.fuel = 0

        self.start = None
        self.direction = None

    def accel_x(self, ax, thrust):
        self.rotation = int(math.asin(-ax/thrust)*180/math.pi)
        print(self.rotation, file=sys.stderr)
        self.thrust = thrust

N = int(input()) # the number of points used to draw the surface of Mars.

def main():
    planet = Planet()

    for _ in range(N):
        # LAND_X: X coordinate of a surface point. (0 to 6999)
        # LAND_Y: Y coordinate of a surface point. By linking all the points
        #   together in a sequential fashion, you form the surface of Mars.
        land_x, land_y = [int(i) for i in input().split()]
        planet.add_point(land_x, land_y)

    ship = Ship()
    while 1:
        main_loop(planet, ship)

def main_loop(planet, ship):
    # HS: the horizontal speed (in m/s), can be negative.
    # VS: the vertical speed (in m/s), can be negative.
    # F: the quantity of remaining fuel in liters.
    # R: the rotation angle in degrees (-90 to 90).
    # P: the thrust power (0 to 4).
    (
        ship.position.x,  # m
        ship.position.y,  # m
        ship.speed.x,     # m/s
        ship.speed.y,     # m/s
        ship.fuel,        # L
        ship.rotation,    # degrees (-90..90)
        ship.thrust,      # thrust (0..4)
    ) = [int(i) for i in input().split()]

    abs_speed = abs(ship.speed.x)
    planet_right = planet.flat_right.x
    planet_left = planet.flat_left.x
    normal = (planet_right-ship.position.x)/abs(planet_right-ship.position.x)

    if ship.position.x > planet_right + 1000 or ship.position.x < planet_left - 1000:
        go_fast(ship, abs_speed, normal)
    elif ship.position.x > planet_right + 500 or ship.position.x < planet_left - 500:
        maintain_x_speed(50, 75, ship, abs_speed, normal, 3)
    elif ship.position.x > planet_right + 200 or ship.position.x < planet_left - 200:
        maintain_x_speed(20, 40, ship, abs_speed, normal, 2)
    elif ship.position.x > planet_right or ship.position.x < planet_left:
        maintain_x_speed(10, 20, ship, abs_speed, normal, 2)
    elif ship.position.x < planet_right and ship.position.x > planet_left:
        if ship.speed.y < -29:
            ship.accel_x(0, 4)
        elif abs_speed > 0:
            ship.accel_x(-1*normal, 4)
        else:
            ship.rotation = 0
            ship.thrust = 0

    # R P. R is the desired rotation angle. P is the desired thrust power.
    print("%d %d" % (ship.rotation, ship.thrust))

def go_fast(ship, abs_speed, normal):
    # go fast
    if abs_speed > 100:
        ship.accel_x(3*normal, 4)
    else:
        ship.accel_x(0, 4)

# pylint: disable=too-many-arguments
def maintain_x_speed(min_speed, max_speed, ship, abs_speed, normal, thrust):
    # stay between 50 and 75
    if abs_speed > min_speed:
        ship.accel_x(1*normal, thrust)
    elif abs_speed > max_speed:
        ship.accel_x(-1*normal, thrust)
    else:
        ship.accel_x(0, thrust)
# pylint: enable=too-many-arguments

if __name__ == '__main__':
    main()
