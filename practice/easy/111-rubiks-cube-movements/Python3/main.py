# pylint: disable=too-few-public-methods

class Face():
    ROTATIONS = {
        'x': {
            'F': 'U',
            'U': 'B',
            'B': 'D',
            'D': 'F',
        },
        'y': {
            'F': 'L',
            'L': 'B',
            'B': 'R',
            'R': 'F',
        },
        'z': {
            'U': 'R',
            'R': 'D',
            'D': 'L',
            'L': 'U',
        },
    }

    def __init__(self, position):
        self.position = position

    def rotate(self, rot):
        rotation_map = self.ROTATIONS[rot]
        try:
            self.position = rotation_map[self.position]
        except KeyError:
            pass # no rotation

def main():
    for rot in list(Face.ROTATIONS.keys()):
        rot_prime = rot + "'"
        Face.ROTATIONS[rot_prime] = {v: k for k, v in Face.ROTATIONS[rot].items()}

    rotations = input().split(' ')
    face_1 = Face(input())
    face_2 = Face(input())

    for rot in rotations:
        face_1.rotate(rot)
        face_2.rotate(rot)

    print(face_1.position)
    print(face_2.position)

if __name__ == '__main__':
    main()
