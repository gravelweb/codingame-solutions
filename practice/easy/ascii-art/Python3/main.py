import string

L = int(input())
H = int(input())
T = input()

ALPHABET = string.ascii_uppercase + '?'

def main():
    letters = {}

    for _ in range(H):
        row = input()
        for i, letter in enumerate(ALPHABET):
            if ALPHABET[i] not in letters:
                letters[ALPHABET[i]] = []
            letters[ALPHABET[i]].append(row[i*L: i*L+L])

    result = ''
    for j in range(H):
        for i, letter in enumerate(T):
            letter = letter.upper()
            if letter not in string.ascii_uppercase:
                letter = '?'
            result += letters[letter][j]
        result += '\n'

    print(result)

if __name__ == '__main__':
    main()
