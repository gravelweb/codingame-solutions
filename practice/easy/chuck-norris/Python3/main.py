import sys

# ¯\_(ツ)_/¯ ... super legacy: leaving this untouched

ZEROS = 0
ONES = 1

MESSAGE = input()
ANSWER = ""

print(MESSAGE, file=sys.stderr)

# 1000000 = 64
# 0100000 = 32
# 0010000 = 16
# 0001000 = 8
# 0000100 = 4
# 0000010 = 2
# 0000001 = 1

def do_it(ascii, position=None, current=None):
    global ANSWER
    if position == -1:
        return
    flag = ONES if ascii - pow(2,position) >= 0 else ZEROS
    if flag == current:
        ANSWER += "0"
    elif flag == ZEROS:
        current = ZEROS
        ANSWER += " 00 0"
    elif flag == ONES:
        current = ONES
        ANSWER += " 0 0"

    if flag == ONES:
        ascii -= pow(2,position)
    do_it(ascii, position-1, current)

ascii = 0
for char in MESSAGE:
    ascii = ascii << 7
    ascii += ord(char)

do_it(ascii, len(MESSAGE)*7 - 1)

#0x43
#0b1000011
#0 0 00 0000 0 00

#0x21C3
#0b100001 11000011
#0 0 00 0000 0 000 00 0000 0 00 

print(ANSWER[1:])
