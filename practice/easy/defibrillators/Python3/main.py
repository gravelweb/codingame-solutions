import math

# pylint: disable=invalid-name

LON = float(input().replace(',', '.')) * math.pi/180
LAT = float(input().replace(',', '.')) * math.pi/180
N = int(input())
EARTH_RADIUS = 6371

def main():
    smallest = [None, None]

    for _ in range(N):
        defib = input()
        split = defib.split(';')
        name = split[1]
        lon = float(split[4].replace(',', '.')) * math.pi/180
        lat = float(split[5].replace(',', '.')) * math.pi/180
        x = (lon - LON) * math.cos((LAT + lat)/2)
        y = lat - LAT
        d = math.sqrt(pow(x, 2) + pow(y, 2)) * EARTH_RADIUS
        if smallest[0] is None or smallest[0] > d:
            smallest[0] = d
            smallest[1] = name

    print(smallest[1])

if __name__ == '__main__':
    main()
