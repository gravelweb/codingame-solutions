#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int LX; // the X position of the light of power
    int LY; // the Y position of the light of power
    int TX; // Thor's starting X position
    int TY; // Thor's starting Y position
    cin >> LX >> LY >> TX >> TY; cin.ignore();

    // game loop
    while (1) {
        int E; // The level of Thor's remaining energy, representing the number of moves he can still make.
        string dir;
        cin >> E; cin.ignore();

        // Write an action using cout. DON'T FORGET THE "<< endl"
        // To debug: cerr << "Debug messages..." << endl;
        if (LY - TY > 0 && TY + 1 < 18) {
            TY += 1;
            dir += "S";
        } else if (LY - TY < 0 && TY - 1 >= 0) {
            TY -= 1;
            dir += "N";
        } if (LX - TX > 0 && TX + 1 < 40) {
            TX += 1;
            dir += "E";
        } else if (LX - TX < 0 && TX - 1 >= 0) {
            TX += 1;
            dir += "W";
        }

        cout << dir << endl; // A single line providing the move to be made: N NE E SE S SW W or NW
    }
}
