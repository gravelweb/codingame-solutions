import os.path

N = int(input()) # Number of elements which make up the association table.
Q = int(input()) # Number Q of file names to be analyzed.

def main():
    mime_types = {}
    for _ in range(N):
        # EXT: file extension
        # MT: MIME type.
        ext, mime_type = input().split()
        mime_types[ext.lower()] = mime_type

    output = []
    for _ in range(Q):
        fname = input() # One file name per line.
        if fname.startswith('.'):
            fname = 'a.' + fname[1:]
        ext = os.path.splitext(fname)[1][1:].lower()
        output.append(mime_types.get(ext, 'UNKNOWN'))

    for answer in output:
        print(answer)

if __name__ == '__main__':
    main()
