#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main()
{
    int N; // the number of temperatures to analyse
    cin >> N; cin.ignore();
    string TEMPS; // the N temperatures expressed as integers ranging from -273 to 5526
    getline(cin, TEMPS);
    
    int lowestTemp;
    if (TEMPS.empty()) {
        lowestTemp = 0;
    } else {
    
        istringstream stream(TEMPS);
        stream >> lowestTemp;
        for (int i = 1; i < N; i++) {
            int temp;
            stream >> temp;
            if (abs(temp) < abs(lowestTemp)) {
                lowestTemp = temp;
            } else if (abs(temp) == abs(lowestTemp)) {
                lowestTemp = max(temp, lowestTemp);   
            }
        }
    }

    // Write an action using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;

    cout << lowestTemp << endl;
}
