# ¯\_(ツ)_/¯ ... super legacy: leaving this untouched

file="strengths.txt"
rm -f $file

read N

for (( i=0; i<N; i++ )); do
    read Pi
    echo $Pi >> "${file}"
done

sort -g -o ${file} ${file}

min_so_far=10000001
last_line=""
while read -r line; do
    if [ -z ${last_line} ]; then
        last_line=${line}
        continue
    fi
    diff=$((line-last_line))
    if [[ ${diff} -lt ${min_so_far} ]]; then
        min_so_far=${diff}
        if [ ${min_so_far} -eq 0 ]; then
            break
        fi
    fi
    last_line=${line}
done <${file}

echo ${min_so_far}
